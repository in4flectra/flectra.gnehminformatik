import uuid
import vobject

from dateutil import parser
from webdav3.client import Client
from xml.etree import ElementTree

options = {
    'webdav_hostname': 'http://nextcloud.example.com',
    'webdav_login':    'nextcloud',
    'webdav_password': 'nextcloud',
}

client = Client(options)
client.verify = False # To not check SSL certificates (Default = True)
# client.session.proxies(...) # To set proxy directly into the session (Optional)
# client.session.auth(...) # To set proxy auth directly into the session (Optional)
new = vobject.vCard()
new.add('fn')
new.fn.value = 'Testfirma (fn)'
new.add('org')
new.org.value = ['Testfirma (org)']
adr = new.add('adr')
adr.type_param = 'WORK'
adr.value = vobject.vcard.Address(street='Teststrasse WORK')
# adr = new.add('adr')
# adr.type_param = 'HOME'
# adr.value = vobject.vcard.Address(street='Teststrasse HOME')
# path = '/remote.php/dav/addressbooks/users/nextcloud/kontakte/' + str(uuid.uuid4()).upper() + '.vcf'
path = '/remote.php/dav/addressbooks/users/nextcloud/kontakte/75434CFE-D80E-4293-8117-67679B19F778.vcf'
response = client.execute_request('upload', path, data=new.serialize())


response = client.execute_request('list', '/remote.php/dav/addressbooks/users/nextcloud/kontakte')
root = ElementTree.fromstring(response.text)
addresses = []
for part in root:
    href = ''
    etag = ''
    for child in part:
        if child.tag == '{DAV:}href':
            href = child.text
        if child.tag == '{DAV:}propstat':
            for propstat in child:
                if propstat.tag == '{DAV:}prop':
                    for prop in propstat:
                        if prop.tag == '{DAV:}getetag':
                            etag = prop.text
    if href and etag:
        addresses.append({'href': href, 'etag': etag})

for address in addresses:
    print(address['href'])
    response = client.execute_request('download', address['href'])
    vcard = vobject.readOne(response.text)

    for n in vcard.contents.get('n', {}):
        if n.value.family and n.value.given:
            print('N: ' + n.value.family + ' ' + n.value.given)
        elif n.value.family:
            print('N: ' + n.value.family)
        elif n.value.given:
            print('N: ' + n.value.given)

    for org in vcard.contents.get('org', {}):
        for org_name in org.value:
            print('Org: ' + org_name)

    for rev in vcard.contents.get('rev', {}):
        # last_update = parser.parse(rev.value)
        print('Rev: ' + rev.value)

    for categories in vcard.contents.get('categories', {}):
        for category in categories.value:
            print('Category: ' + category)


    for fn in vcard.contents.get('fn', {}):
        print(fn.value)
    for adr in vcard.contents.get('adr', {}):
        if adr.value:
            print(adr.params['TYPE'][0] + '-Address:')
        if adr.value.box:
            print(adr.value.box)
        if adr.value.street:
            print(adr.value.street)
        if adr.value.extended:
            print(adr.value.extended)
        if adr.value.code and adr.value.city:
            print(adr.value.code + ' ' + adr.value.city)
        elif adr.value.code:
            print(adr.value.code)
        elif adr.value.city:
            print(adr.value.city)
        if adr.value.region:
            print(adr.value.region)
        if adr.value.country:
            print(adr.value.country)
    for email in vcard.contents.get('email', {}):
        if email.value:
            print(email.params['TYPE'][0] + '-E-Mail: ' + email.value)
    for tel in vcard.contents.get('tel', {}):
        if tel.value:
            print(tel.params['TYPE'][0] + '-Phone: ' + tel.value)
