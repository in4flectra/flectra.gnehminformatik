#!/bin/bash

if [ -d "./../flectra" ]; then
    find ./../flectra -type d -exec chmod 755 {} +
    find ./../flectra -type f -exec chmod 644 {} +
fi

