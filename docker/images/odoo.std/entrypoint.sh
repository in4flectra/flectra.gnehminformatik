#!/bin/bash


# PostgreSQL
chown postgres:postgres -R /var/lib/postgresql/data/

if [ ! -d "/var/lib/postgresql/data/main/" ]; then
    pg_dropcluster --stop $POSTGRESQL_VERSION main
    pg_createcluster --locale de_CH.UTF-8 --start $POSTGRESQL_VERSION main
fi

sed -i -e "s,data_directory = '/var/lib/postgresql/$POSTGRESQL_VERSION/main',data_directory = '/var/lib/postgresql/data/main'," /etc/postgresql/$POSTGRESQL_VERSION/main/postgresql.conf

if [ ! -d "/var/lib/postgresql/data/main/" ]; then
    rsync -av -q /var/lib/postgresql/$POSTGRESQL_VERSION/main /var/lib/postgresql/data
    su postgres -c "/etc/init.d/postgresql start"
    su postgres -c "/etc/init.d/postgresql stop"
fi

su postgres -c "/etc/init.d/postgresql start"


# Odoo
chown odoo:odoo -R /usr/lib/python3/dist-packages/odoo/.local/share/Odoo/

if [ ! -f "/etc/odoo/odoo.conf" ]; then
    mv /usr/lib/python3/dist-packages/odoo/odoo.conf /etc/odoo/odoo.conf

    echo "CREATE USER odoo WITH SUPERUSER PASSWORD 'odoo';" > /tmp/create_user.sql
    su postgres -c "psql -f /tmp/create_user.sql"
    rm /tmp/create_user.sql
fi

if [ ! -d "/etc/odoo/backup/" ]; then
    mkdir -p /etc/odoo/backup/
    chown -R odoo:odoo /etc/odoo/backup/
fi

if [ ! -d "/etc/odoo/logfiles/" ]; then
    mkdir -p /etc/odoo/logfiles/
    chown -R odoo:odoo /etc/odoo/logfiles/
fi

if [ -f "/usr/lib/python3/dist-packages/odoo/addons_customer/requirements.txt" ]; then
    echo "Installing Customer Requirements ..."
    pip3 install -r /usr/lib/python3/dist-packages/odoo/addons_customer/requirements.txt
fi

echo "Odoo is up and running ..."
su odoo -c "/usr/lib/python3/dist-packages/odoo/odoo-bin --config /etc/odoo/odoo.conf --without-demo=all --logfile=/etc/odoo/logfiles/odoo.log"
