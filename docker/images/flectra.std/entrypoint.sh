#!/bin/bash


# PostgreSQL
chown postgres:postgres -R /var/lib/postgresql/data/

if [ ! -d "/var/lib/postgresql/data/main/" ]; then
    pg_dropcluster --stop $POSTGRESQL_VERSION main
    pg_createcluster --locale de_CH.UTF-8 --start $POSTGRESQL_VERSION main
fi

sed -i -e "s,data_directory = '/var/lib/postgresql/$POSTGRESQL_VERSION/main',data_directory = '/var/lib/postgresql/data/main'," /etc/postgresql/$POSTGRESQL_VERSION/main/postgresql.conf

if [ ! -d "/var/lib/postgresql/data/main/" ]; then
    rsync -av -q /var/lib/postgresql/$POSTGRESQL_VERSION/main /var/lib/postgresql/data
    su postgres -c "/etc/init.d/postgresql start"
    su postgres -c "/etc/init.d/postgresql stop"
fi

su postgres -c "/etc/init.d/postgresql start"


# Flectra
chown flectra:flectra -R /usr/lib/python3/dist-packages/flectra/.local/share/Flectra/

if [ ! -f "/etc/flectra/flectra.conf" ]; then
    mv /usr/lib/python3/dist-packages/flectra/flectra.conf /etc/flectra/flectra.conf

    echo "CREATE USER flectra WITH SUPERUSER PASSWORD 'flectra';" > /tmp/create_user.sql
    su postgres -c "psql -f /tmp/create_user.sql"
    rm /tmp/create_user.sql
fi

if [ ! -d "/etc/flectra/backup/" ]; then
    mkdir -p /etc/flectra/backup/
    chown -R flectra:flectra /etc/flectra/backup/
fi

if [ ! -d "/etc/flectra/logfiles/" ]; then
    mkdir -p /etc/flectra/logfiles/
    chown -R flectra:flectra /etc/flectra/logfiles/
fi

if [ -f "/usr/lib/python3/dist-packages/flectra/addons_customer/requirements.txt" ]; then
    echo "Installing Customer Requirements ..."
    pip3 install -r /usr/lib/python3/dist-packages/flectra/addons_customer/requirements.txt
fi

echo "Flectra is up and running ..."
su flectra -c "/usr/lib/python3/dist-packages/flectra/flectra-bin --config /etc/flectra/flectra.conf --without-demo=all --logfile=/etc/flectra/logfiles/flectra.log"
