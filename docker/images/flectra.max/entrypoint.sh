#!/bin/bash


# PostgreSQL
chown postgres:postgres -R /var/lib/postgresql/data/

if [ ! -d "/var/lib/postgresql/data/main/" ]; then
    pg_dropcluster --stop $POSTGRESQL_VERSION main
    pg_createcluster --locale de_CH.UTF-8 --start $POSTGRESQL_VERSION main
fi

sed -i -e "s,data_directory = '/var/lib/postgresql/$POSTGRESQL_VERSION/main',data_directory = '/var/lib/postgresql/data/main'," /etc/postgresql/$POSTGRESQL_VERSION/main/postgresql.conf

if [ ! -d "/var/lib/postgresql/data/main/" ]; then
    rsync -av -q /var/lib/postgresql/$POSTGRESQL_VERSION/main /var/lib/postgresql/data
    su postgres -c "/etc/init.d/postgresql start"
    su postgres -c "/etc/init.d/postgresql stop"
fi

su postgres -c "/etc/init.d/postgresql start"


# Nextcloud
sed -i "s/<Nextcloud-Domain-Name>/$NEXTCLOUD_DOMAIN_NAME/g" /etc/nginx/sites-available/nextcloud.conf

if [ ! -f "/etc/nginx/sites-enabled/nextcloud.conf" ]; then
    chown root:root /etc/nginx/sites-available/nextcloud.conf
    chmod -R 644 /etc/nginx/sites-available/nextcloud.conf
    ln -s /etc/nginx/sites-available/nextcloud.conf /etc/nginx/sites-enabled/nextcloud.conf
fi

if [ ! -f "/var/www/nextcloud/config/config.sample.php" ]; then
    cp -r /var/www/nextcloud/config.org/* /var/www/nextcloud/config/
    cp -r /var/www/nextcloud/apps.org/*   /var/www/nextcloud/apps/
    cp -r /var/www/nextcloud/themes.org/* /var/www/nextcloud/themes/

    chown www-data:www-data -R /var/www/nextcloud/config/
    chown www-data:www-data -R /var/www/nextcloud/apps/
    chown www-data:www-data -R /var/www/nextcloud/data/
    chown www-data:www-data -R /var/www/nextcloud/themes/

    echo "CREATE USER nextcloud WITH PASSWORD 'nextcloud';" > /tmp/create_user.sql
    su postgres -c "psql -f /tmp/create_user.sql"
    rm /tmp/create_user.sql

    su postgres -c "psql -c 'CREATE DATABASE nextcloud;'"
    su postgres -c "psql -c 'ALTER DATABASE nextcloud OWNER TO nextcloud;'"
    su postgres -c "psql -c 'GRANT ALL PRIVILEGES ON DATABASE nextcloud TO nextcloud;'"
fi

if [ -d "/var/www/nextcloud/config.org/" ]; then
    rm -r /var/www/nextcloud/config.org/
fi

if [ -d "/var/www/nextcloud/apps.org/" ]; then
    rm -r /var/www/nextcloud/apps.org/
fi

if [ -d "/var/www/nextcloud/themes.org/" ]; then
    rm -r /var/www/nextcloud/themes.org/
fi

if [ ! -d "/etc/nextcloud/backup/" ]; then
    mkdir -p /etc/nextcloud/backup/
    chown -R postgres:postgres /etc/nextcloud/backup/
fi

cron

echo "*/5  *  *  *  * php -f /var/www/nextcloud/cron.php >> /var/www/nextcloud/data/background.log 2>&1" > background_cron
crontab -u www-data background_cron
rm background_cron

if [ -v NEXTCLOUD_BACKUP_SERVER ] && [ -v NEXTCLOUD_BACKUP_USER ]; then
    if [ -f "/root/.ssh/known_hosts" ]; then
        ssh-keyscan -H $NEXTCLOUD_BACKUP_SERVER >> /root/.ssh/known_hosts
    else
        mkdir -p ~/.ssh/
        ssh-keyscan -H $NEXTCLOUD_BACKUP_SERVER > /root/.ssh/known_hosts
    fi
    echo "$NEXTCLOUD_BACKUP_TIME /etc/nextcloud/backup.sh $NEXTCLOUD_BACKUP_SERVER $NEXTCLOUD_BACKUP_USER >> /var/www/nextcloud/data/backup.log 2>&1" > backup_cron
    crontab backup_cron
    rm backup_cron
else
    echo "$NEXTCLOUD_BACKUP_TIME /etc/nextcloud/backup.sh >> /var/www/nextcloud/data/backup.log 2>&1" > backup_cron
    crontab backup_cron
    rm backup_cron
fi

service nginx stop
echo Stopping php$PHP_VERSION-fpm ...
service php$PHP_VERSION-fpm stop

echo Starting php$PHP_VERSION-fpm ...
service php$PHP_VERSION-fpm start
service nginx start


# Flectra
chown flectra:flectra -R /usr/lib/python3/dist-packages/flectra/.local/share/Flectra/

if [ ! -f "/etc/flectra/flectra.conf" ]; then
    mv /usr/lib/python3/dist-packages/flectra/flectra.conf /etc/flectra/flectra.conf

    echo "CREATE USER flectra WITH SUPERUSER PASSWORD 'flectra';" > /tmp/create_user.sql
    su postgres -c "psql -f /tmp/create_user.sql"
    rm /tmp/create_user.sql
fi

if [ ! -d "/etc/flectra/backup/" ]; then
    mkdir -p /etc/flectra/backup/
    chown -R flectra:flectra /etc/flectra/backup/
fi

if [ ! -d "/etc/flectra/logfiles/" ]; then
    mkdir -p /etc/flectra/logfiles/
    chown -R flectra:flectra /etc/flectra/logfiles/
fi

if [ -f "/usr/lib/python3/dist-packages/flectra/addons_customer/requirements.txt" ]; then
    echo "Installing Customer Requirements ..."
    pip3 install -r /usr/lib/python3/dist-packages/flectra/addons_customer/requirements.txt
fi

echo "Flectra is up and running ..."
su flectra -c "/usr/lib/python3/dist-packages/flectra/flectra-bin --config /etc/flectra/flectra.conf --without-demo=all --logfile=/etc/flectra/logfiles/flectra.log"
