#!/bin/bash

echo --------------------------------------
echo Startet on $(date)
echo --------------------------------------
cd /var/www/nextcloud/
sudo -u www-data php occ maintenance:mode --on

rm /etc/nextcloud/backup/* 2>/dev/null

echo Create SQL-Dump of Nextcloud Database
su postgres -c "pg_dump nextcloud > /etc/nextcloud/backup/dump.sql"
mv /etc/nextcloud/backup/dump.sql /var/www/nextcloud/dump.sql

echo Create ZIP of Nextcloud Config, Apps, Data and Themes
rm -f data/nextcloud.log.*
zip -q -r /etc/nextcloud/backup/`date +"%Y_%m_%d_%H_%M_%S"`_nextcloud.zip dump.sql config/ apps/ data/ themes/
rm /var/www/nextcloud/dump.sql

cd /var/www/nextcloud/
sudo -u www-data php occ maintenance:mode --off

if [ $# -eq 2 ] && [ -f "/var/www/nextcloud/config/sftp.password" ]; then
    echo Copy ZIP to SFTP
    sshpass -f /var/www/nextcloud/config/sftp.password sftp -oBatchMode=no -b /var/www/nextcloud/config/sftp.directory $2@$1 2>/dev/null
    sshpass -f /var/www/nextcloud/config/sftp.password sftp -oBatchMode=no -b /var/www/nextcloud/config/sftp.backup    $2@$1
elif [ -d "/etc/flectra/backup/" ]; then
    cp /etc/nextcloud/backup/* /etc/flectra/backup/
fi

echo Finished on $(date)
echo
