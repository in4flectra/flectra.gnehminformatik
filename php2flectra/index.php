<?php

echo '<title>PHP2Flectra</title>';

require_once('ripcord/ripcord.php');

$url = 'http://flectra.example.com:7073';
$db = 'php2flectra';
$username = 'admin';
$password = 'admin';

# ' --> \'
# \ --> \\
# " --> " (no escape)

try {
    $common = ripcord::client($url . '/xmlrpc/2/common');
    $version = $common->version();
} catch (Exception $e) {
    echo 'Exception: ', $e->getMessage(), "\n";
}
echo 'Version:';
echo '<pre>';
print_r($version);
echo '</pre>';

$uid = $common->authenticate($db, $username, $password, array());
echo 'UID: ' . $uid . '<br>';


$model = 'res.partner';
$fields = array('name', 'street', 'zip', 'city', 'country_id');
$models = ripcord::client($url . '/xmlrpc/2/object');
$has_read_access = $models->execute_kw($db, $uid, $password, $model, 'check_access_rights', array('read'), array('raise_exception' => false));
if ($has_read_access == 1):
    echo '<br>';
    echo 'Has read access to "' . $model . '": Yes<br>';

    # $ids = $models->execute_kw($db, $uid, $password, $model, 'search', array(array()));
    # $ids = $models->execute_kw($db, $uid, $password, $model, 'search', array(array(array('is_company', '=', true), array('customer', '=', true))));
    $ids = $models->execute_kw($db, $uid, $password, $model, 'search', array(array(array('is_company', '=', true), array('parent_id', '=', False))));
    echo '<br>';
    echo 'IDs found in "' . $model . '":';
    echo '<pre>';
    print_r($ids);
    echo '</pre>';

    $records = $models->execute_kw($db, $uid, $password, $model, 'read', array($ids), array('fields'=>$fields));
    echo '<br>';
    echo 'Records of found "' . $model . '"-IDs:';
    echo '<pre>';
    print_r($records);
    echo '</pre>';
else:
    echo 'Has read access to "' . $model . '": No<br>';
endif;
