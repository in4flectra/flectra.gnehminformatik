#!/bin/bash

ext=
check_only=false
if [[ $# -eq 1 ]]; then
    if [[ $1 == "check" ]]; then
        check_only=true
    else
        ext=.$1
    fi
fi
if [[ $# -eq 2 ]]; then
    if [[ $1 == "check" ]]; then
        check_only=true
    else
        ext=.$1
    fi
    if [[ $2 == "check" ]]; then
        check_only=true
    else
        ext=.$2
    fi
fi

cd ..

if [[ -d "flectra$ext" ]]; then
    echo flectra$ext:
    cd flectra$ext/
    git pull
else
    if [[ $ext == ".hq" ]]; then
        git clone https://gitlab.com/flectra-hq/flectra.git --branch master flectra$ext
    elif [[ $ext == ".master" ]]; then
        git clone https://gitlab.com/in4flectra/flectra.git --branch master flectra$ext
    elif [[ $ext == ".1.6.1" ]]; then
        git clone https://gitlab.com/in4flectra/flectra.git --branch 1.6.1 flectra$ext
    elif [[ $ext == ".1.6.2" ]]; then
        git clone https://gitlab.com/in4flectra/flectra.git --branch 1.6.2 flectra$ext
    elif [[ $ext == ".1.7.5" ]]; then
        git clone https://gitlab.com/in4flectra/flectra.git --branch 1.7.5 flectra$ext
    else
        git clone https://gitlab.com/in4flectra/flectra.git --branch dev flectra$ext
    fi
    cd flectra$ext
fi
git config credential.helper store
if ! $check_only; then
    sudo pip3 uninstall docutils-stubs -y
    [[ -f requirements.txt ]] && sudo pip3 install -r requirements.txt
fi
cd ..
echo


if [[ $ext != ".hq" ]] && [[ $ext != ".master" ]]; then
    if [[ -d "flectra$ext/addons_other" ]]; then
        echo flectra$ext/addons_other:
        cd flectra$ext/addons_other/
        git pull
    else
        cd flectra$ext/
        if [[ $ext == ".1.6.1" ]]; then
            git clone https://gitlab.com/in4flectra/addons_other.git --branch 1.6.1 addons_other
        elif [[ $ext == ".1.6.2" ]]; then
            git clone https://gitlab.com/in4flectra/addons_other.git --branch 1.6.2 addons_other
        elif [[ $ext == ".1.7.5" ]]; then
            git clone https://gitlab.com/in4flectra/addons_other.git --branch 1.7.5 addons_other
        else
            git clone https://gitlab.com/in4flectra/addons_other.git --branch master addons_other
        fi
        cd addons_other
    fi
    git config credential.helper store
    if ! $check_only; then
        [[ -f requirements.txt ]] && sudo pip3 install -r requirements.txt
    fi
    cd ../..
    echo
fi


if [[ $ext != ".hq" ]] && [[ $ext != ".master" ]]; then
    if [[ -d "flectra$ext/addons_gnehminformatik" ]]; then
        echo flectra$ext/addons_gnehminformatik:
        cd flectra$ext/addons_gnehminformatik/
        git pull
    else
        cd flectra$ext/
        if [[ $ext == ".1.6.1" ]]; then
            git clone https://gitlab.com/in4flectra/addons_gnehminformatik.git --branch 1.6.1 addons_gnehminformatik
        elif [[ $ext == ".1.6.2" ]]; then
            git clone https://gitlab.com/in4flectra/addons_gnehminformatik.git --branch 1.6.2 addons_gnehminformatik
        elif [[ $ext == ".1.7.5" ]]; then
            git clone https://gitlab.com/in4flectra/addons_gnehminformatik.git --branch 1.7.5 addons_gnehminformatik
        else
            git clone https://gitlab.com/in4flectra/addons_gnehminformatik.git --branch master addons_gnehminformatik
        fi
        cd addons_gnehminformatik
    fi
    git config credential.helper store
    if ! $check_only; then
        [[ -f requirements.txt ]] && sudo pip3 install -r requirements.txt
    fi
    cd ../..
    echo
fi


if [[ $ext != ".hq" ]] && [[ $ext != ".master" ]]; then
    if [[ -d "flectra$ext/addons_customer" ]]; then
        echo flectra$ext/addons_customer:
        cd flectra$ext/addons_customer/
        git pull
    else
        cd flectra$ext/
        if [[ $ext == ".1.6.1" ]]; then
            git clone https://gitlab.com/in4flectra/addons_customer.git --branch 1.6.1 addons_customer
        elif [[ $ext == ".1.6.2" ]]; then
            git clone https://gitlab.com/in4flectra/addons_customer.git --branch 1.6.2 addons_customer
        elif [[ $ext == ".1.7.5" ]]; then
            git clone https://gitlab.com/in4flectra/addons_customer.git --branch 1.7.5 addons_customer
        else
            git clone https://gitlab.com/in4flectra/addons_customer.git --branch master addons_customer
        fi
        cd addons_customer
    fi
    git config credential.helper store
    if ! $check_only; then
        [[ -f requirements.txt ]] && sudo pip3 install -r requirements.txt
        for dir in ./*/; do
            dir=${dir%*/}
            cd ${dir##*/}
            [[ -f requirements.txt ]] && sudo pip3 install -r requirements.txt
            cd ..
        done
    fi
    cd ../..
    echo
fi


if [[ $ext != ".hq" ]] && [[ $ext != ".master" ]]; then
    if [[ -d "flectra.gnehminformatik$ext" ]]; then
        echo flectra.gnehminformatik$ext:
        cd flectra.gnehminformatik$ext/
        git pull
    else
        if [[ $ext == ".1.6.1" ]]; then
            git clone https://gitlab.com/in4flectra/flectra.gnehminformatik.git --branch 1.6.2 flectra.gnehminformatik$ext
        elif [[ $ext == ".1.6.2" ]]; then
            git clone https://gitlab.com/in4flectra/flectra.gnehminformatik.git --branch 1.6.2 flectra.gnehminformatik$ext
        elif [[ $ext == ".1.7.5" ]]; then
            git clone https://gitlab.com/in4flectra/flectra.gnehminformatik.git --branch 1.7.5 flectra.gnehminformatik$ext
        else
            git clone https://gitlab.com/in4flectra/flectra.gnehminformatik.git --branch master flectra.gnehminformatik$ext
        fi
        cd flectra.gnehminformatik$ext
    fi
    git config credential.helper store
    if ! $check_only; then
        [[ -f requirements.txt ]] && sudo pip3 install -r requirements.txt
    fi
    cd ..
    echo
fi
