# Tools and Data for Flectra

<h2>Docker: Only Flectra</h2>
<b>Create Image</b><br>
cd flectra.min/<br>
docker build -t flectra.min .<br>
<br>
<b>Create Container</b><br>
cd containers/<br>
docker run --detach \ <br>
--name &lt;Container Name&gt; \ <br>
--publish 127.0.0.1:&lt;Flectra-Port&gt;:7073 \ <br>
--volume ${PWD}/&lt;Container Name&gt;/postgres-data:/var/lib/postgresql/data/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-config:/etc/flectra/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-addons:/usr/lib/python3/dist-packages/flectra/addons_customer/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-data:/usr/lib/python3/dist-packages/flectra/.local/share/Flectra/ \ <br>
flectra.min<br>


<h2>Docker: Flectra with Postfix</h2>
<b>Create Image</b><br>
cd flectra.std/<br>
docker build -t flectra.std .<br>
<br>
<b>Create Container</b><br>
cd containers/<br>
docker run --detach \ <br>
--name &lt;Container Name&gt; \ <br>
--env POSTFIX_REDIRECT_MAIL=&lt;E-Mail&gt; \ <br>
--publish 127.0.0.1:&lt;Flectra-Port&gt;:7073 \ <br>
--volume ${PWD}/&lt;Container Name&gt;/postgres-data:/var/lib/postgresql/data/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-config:/etc/flectra/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-addons:/usr/lib/python3/dist-packages/flectra/addons_customer/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-data:/usr/lib/python3/dist-packages/flectra/.local/share/Flectra/ \ <br>
flectra.std<br>


<h2>Docker: Flectra with Postfix and Nextcloud</h2>
<b>Create Image</b><br>
cd flectra.max/<br>
docker build -t flectra.max .<br>
<br>
<b>Create Container</b><br>
cd containers/<br>
docker run --detach \ <br>
--name &lt;Container Name&gt; \ <br>
--env POSTFIX_REDIRECT_MAIL=&lt;E-Mail&gt; \ <br>
--env NEXTCLOUD_BACKUP_SERVER=&lt;SFTP-Server-IP-Address&gt; \
--env NEXTCLOUD_BACKUP_USER=&lt;SFTP-Server-Username&gt; \
--env NEXTCLOUD_BACKUP_TIME='&lt;Cronjob-Execution-Time e.g. 30 */4 * * *&gt;' \
--publish 127.0.0.1:&lt;Flectra-Port&gt;:7073 \ <br>
--publish 127.0.0.1:&lt;Nextcloud-Port&gt;:80 \ <br>
--volume ${PWD}/&lt;Container Name&gt;/postgres-data:/var/lib/postgresql/data/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-config:/etc/flectra/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-addons:/usr/lib/python3/dist-packages/flectra/addons_customer/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/flectra-data:/usr/lib/python3/dist-packages/flectra/.local/share/Flectra/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/nextcloud-config:/var/www/nextcloud/config/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/nextcloud-data:/var/www/nextcloud/data/ \ <br>
--volume ${PWD}/&lt;Container Name&gt;/nextcloud-themes:/var/www/nextcloud/themes/ \ <br>
flectra.max<br>


<h2>Docker: Container</h2>
<b>Prepare</b><br>
CTRL+C<br>
sudo docker ps -a<br>
sudo docker start &lt;ID&gt;<br>
--&gt; Stop (CTRL+C) and Restart is importend!!!<br>
<br>
<b>Flectra Setup</b><br>
*** 127.0.0.1:&lt;Flectra-Port&gt;/ ***<br>
--&gt; Master Password: flectra<br>
--&gt; Database Name: flectra<br>
<br>
<b>Flectra Configuration</b><br>
sudo docker stop &lt;ID&gt;<br>
sudo nano &lt;Container Name&gt;/flectra-config/flectra.conf<br>
--&gt; Change "list_db = True" to "list_db = False"<br>
--&gt; Add: "db_name = flectra"<br>
sudo docker start &lt;ID&gt;<br>
<br>
<b>Flectra Additional Modules</b><br>
Copy the Custom Modules to the "&lt;Container Name&gt;/flectra-addons"-Directory<br>
<br>
<b>Flectra Update</b><br>
docker ps<br>
docker exec -it &lt;ID&gt; bin/bash<br>
ps aux<br>
kill -STOP &lt;Flectra PID&gt;<br>
su - flectra<br>
/usr/lib/python3/dist-packages/flectra/flectra-bin --config=/etc/flectra/flectra.conf --stop-after-init --update all<br>
exit<br>
exit<br>
docker stop &lt;ID&gt;<br>
docker start &lt;ID&gt;<br>
<br>
<b>Nextcloud Setup</b><br>
*** 127.0.0.1:&lt;Nextcloud-Port&gt;/ ***<br>
--&gt; Database User: nextcloud<br>
--&gt; Database Password: nextcloud<br>
--&gt; Database Name: nextcloud<br>
<br>
<b>Nextcloud Configuration</b><br>
sudo docker stop &lt;ID&gt;<br>
sudo nano &lt;Container Name&gt;/nextcloud-config/config.php<br>
--&gt; Add before ");": "&nbsp;&nbsp;'memcache.local' =&gt; '\OC\Memcache\APCu',"<br>
sudo nano &lt;Container Name&gt;/nextcloud-config/sftp.directory<br>
--&gt; Change "&lt;Backup-Directory&gt;"<br>
--&gt; Change "&lt;Sub-Directory&gt;"<br>
sudo nano &lt;Container Name&gt;/nextcloud-config/sftp.backup<br>
--&gt; Change "&lt;Backup-Directory&gt;"<br>
--&gt; Change "&lt;Sub-Directory&gt;"<br>
sudo nano &lt;Container Name&gt;/nextcloud-config/sftp.password<br>
--&gt; Add: "&lt;SFTP-Server-Password&gt;"<br>
sudo docker start &lt;ID&gt;<br>
